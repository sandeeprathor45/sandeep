#!/bin/ksh


Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'

printf "${Cyan}Core:${NC} "
${HOME}/ASMM/Core/bin/xacct test -i;
printf "${Cyan}G01 OFCA:${NC} "
${HOME}/ASMM/G01_OFCA/bin/xacct test -i;
printf "${Cyan}G02 OFCA:${NC} "
${HOME}/ASMM/G02_OFCA/bin/xacct test -i;
printf "${Cyan}G03 OFCA:${NC} "
${HOME}/ASMM/G03_OFCA/bin/xacct test -i;
printf "${Cyan}G04 OFCA:${NC} "
${HOME}/ASMM/G04_OFCA/bin/xacct test -i;
printf "${Cyan}G05 OFCA:${NC} "
${HOME}/ASMM/G05_OFCA/bin/xacct test -i;
printf "${Cyan}G06 OFCA:${NC} "
${HOME}/ASMM/G07_OFCA/bin/xacct test -i;
