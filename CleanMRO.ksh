for q in $(ipcs -qp |grep "${USER} "| awk '{print $1}'  );do
   echo Clearing queue: $q                                  
   ipcrm -q $q                                              
done                                                        
                                                           
for mysem in `ipcs -s | grep "$USER " | awk '{print $2}'`   
do                                                          
        echo "Removing shared memory segments: $mysem"      
        ipcrm -s $mysem                                     
done

