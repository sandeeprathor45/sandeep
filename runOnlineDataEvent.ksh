#!/bin/ksh

#Author : Sandeep Rathor

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'


echo "\n${Cyan}Select Country: ${NC}"
echo "\n${Yellow} 1)Netherlands(National) \n 2)Belgium(EU) \n 3)Germany(EU) \n 4)India(ROW) \n 5)USA(ROW) \n 6)CUBA(Exception) \n 7)Turkey\n${NC}"

read choice
case $choice in
1)
MYADD=196.012.201.089
MYMNCC=20420
;;
2)
MYADD=213.181.060.001
MYMNCC=20601
;;
3)
MYADD=193.254.136.001
MYMNCC=26201
;;
4)
MYADD=202.159.213.049
MYMNCC=40468
;;
5)
MYADD=066.001.065.012
MYMNCC=31012
;;
6)
MYADD=200.013.145.065
MYMNCC=36801
;;
7)
MYADD=212.065.133.033
MYMNCC=28602
;;
8)
MYADD=079.171.051.066
MYMNCC=27602
;;
*)
echo "\n${Red}Sorry ! There is something wrong with your imput....${NC}"
;;
esac

printf "\n${Cyan}Subscriber No: ${NC}"
read MSISDN

printf "\n${Cyan}Data Amount(KB) MAX-800MB: ${NC}"
read AMOUNT
let "BYTE = ( $AMOUNT / 2.0 ) * 1024"

printf "\n${Cyan}No. Of Sessions(MIN. 1): ${NC}"
read NOS

printf "\n${Cyan}Date & Time(YYYYMMDD HH:MM:SS): ${NC}"
read TDTIME
DTIME=`date +%s -ud"$TDTIME"`
while [ $? -eq 1 ]
do
        printf "\n${Red}Incorrect Format ! Please Re-Enter: ${NC}"
        read TDTIME
        DTIME=`date +%s -ud"$TDTIME"`
done
VDTIME=`date -d @$DTIME`
echo "\n${Cyan}Please Verify your Inputs.... ${NC}"

echo "\nMSISDN: ${Yellow}$MSISDN${NC}"
echo "Data Amount(KB): ${Yellow}$AMOUNT${NC}"
echo "No. of Sessions: ${Yellow}$NOS${NC}"
echo "Date and Time: ${Yellow}$VDTIME${NC}"

printf "\n${Cyan}Fine ? (Y/N): ${NC}"
read VERIFY
if [ "$VERIFY" != "Y" ] ; then
	echo "\n${Red}Script Terminated !${NC}"
	exit;
fi

sed -e "s/mysgsnaddress/$MYADD/g ; s/mysgsnmnccvalue/$MYMNCC/g"  EGEN/CUSTOM/CUSTOM_DIAMETER.xml_san > EGEN/CUSTOM/CUSTOM_DIAMETER.xml
sed -e "s/MSISDN/$MSISDN/g"  EGEN/DATA/san.lst > EGEN/DATA/rat.lst
sed -e "s/DTIME/$DTIME/g ; s/DATAVALUE/$BYTE/g ; s/FSOVALUE/$NOS/g"  EGEN/PAR/Gy_NOTIFY_SANITY_temp.par > EGEN/PAR/Gy_NOTIFY_SANITY_san.par
cd EGEN/PAR
/users/gen/$USER/EGSPY/EG.ksh Gy_NOTIFY_SANITY_san.par
